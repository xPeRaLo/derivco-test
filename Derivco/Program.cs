﻿using System;

namespace Question1
{
    class Program
    {
        private static char[] _transcode = new char[65];
        private static readonly string _testString = "This is a test string";
        private static void Prep()
        {
            for (int i = 0; i < _transcode.Length; i++)
            {
                _transcode[i] = (char)((int)'A' + i);

                if (i > 25) 
                {
                    _transcode[i] = (char)((int)_transcode[i] + 6);
                } 
                if (i > 51)
                {
                    _transcode[i] = (char)((int)_transcode[i] - 0x4b);
                } 
            }
            _transcode[62] = '+';
            _transcode[63] = '/';
            _transcode[64] = '=';
        }

        static void Main(string[] args)
        {
            Prep();

            if (Convert.ToBoolean(String.Compare(_testString, Decode(Encode(_testString)))))
            {
                Console.WriteLine("Test succeeded");
            }
            Console.WriteLine("Test failed");

            Console.ReadLine();
        }


        private static string Encode(string input)
        {
            int inputLength = input.Length;
            int cb = (inputLength / 3 + (Convert.ToBoolean(inputLength % 3) ? 1 : 0)) * 4;

            char[] output = new char[cb];
            for (int i = 0; i < cb; i++)
            {
                output[i] = '=';
            }

            int c = 0;
            int reflex = 0;
            const int s = 0x3f;

            for (int j = 0; j < inputLength; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;
                reflex += input[j];

                int x = ((j % 3) + 1) * 2;
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot = (reflex & mask) >> x;
                    output[c++] = _transcode[pivot];
                    int invert = ~mask;
                    reflex &= invert;
                    mask >>= 6;
                    x -= 6;
                }
            }

            switch (inputLength % 3)
            {
                case 1:
                    reflex <<= 4;
                    output[c++] = _transcode[reflex];
                    break;

                case 2:
                    reflex <<= 2;
                    output[c++] = _transcode[reflex];
                    break;

            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }


        private static string Decode(string input)
        {
            int inputLength = input.Length;
            int cb = (inputLength / 3 + (Convert.ToBoolean(inputLength % 3) ? 1 : 0)) * 4;
            char[] output = new char[cb];
            int c = 0;
            int bits = 0;
            int reflex = 0;
            for (int j = 0; j < inputLength; j++)
            {
                reflex <<= 6;
                bits += 6;
                bool fTerminate = ('=' == input[j]);
                if (!fTerminate)
                    reflex += IndexOf(input[j]);

                while (bits >= 8)
                {
                    int mask = 0x000000ff << (bits % 8);
                    output[c++] = (char)((reflex & mask) >> (bits % 8));
                    int invert = ~mask;
                    reflex &= invert;
                    bits -= 8;
                }

                if (fTerminate)
                    break;
            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }

        private static int IndexOf(char ch)
        {            
            for (int i = 0; i < _transcode.Length; i++)
			{
                if (ch == _transcode[i])
				{
                    return i;
				}
            }

            return -1;
        }
    }

    public static class StringExtensions
	{
       /* public static bool IsEqual(this string self, string compareTo)
		{

		}*/
	}
}

