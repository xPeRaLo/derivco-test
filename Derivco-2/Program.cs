﻿using System;
namespace Question2
{
    class Program
    {
        private static readonly int NUMBER_OF_DECKS = 2;
        static void Main(string[] args)
        {
            HighCard highCard = new HighCard(NUMBER_OF_DECKS);

            highCard.Play(highCard);

            Console.ReadLine();
        }
    } 
}