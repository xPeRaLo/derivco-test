﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Question2
{
    public class CardDeck
    {
        private const int MAX_CARD_VALUE = 20;

        private SuitDeck clubDeck;
        private SuitDeck diamondDeck;
        private SuitDeck heartDeck;
        private SuitDeck spadeDeck;

        public CardDeck()
        {
            var suitValues = Enum.GetValues(typeof(CardSuit));

            for (int i = 0; i < suitValues.Length; i++)
            {
                CreateDeck(i);
            }
        }

        public SuitDeck ClubDeck => clubDeck;
        public SuitDeck DiamondDeck => diamondDeck;
        public SuitDeck HeartDeck => heartDeck;
        public SuitDeck SpadeDeck => spadeDeck;

        private void CreateDeck(int suitValue)
        {
            switch (suitValue)
            {
                case 0:
                    FillDeck(CardSuit.CLUBS, out clubDeck);
                    break;
                case 1:
                    FillDeck(CardSuit.DIAMONDS, out diamondDeck);
                    break;
                case 2:
                    FillDeck(CardSuit.HEARTS, out heartDeck);
                    break;
                case 3:
                    FillDeck(CardSuit.SPADES, out spadeDeck);
                    break;
            }
        }

        private void FillDeck(CardSuit suit, out SuitDeck deck)
        {
            bool hasWildcard = false;
            List<Card> cards = new List<Card>();

            Random random = new Random();
            int r = random.Next(1, MAX_CARD_VALUE + 1);

            for (int i = 1; i <= MAX_CARD_VALUE; i++)
            {
                bool isWildcard = false;

                if (!hasWildcard)
                {
                    if (r != i) isWildcard = false;
                    else
                    {
                        isWildcard = true;
                        hasWildcard = true;
                    }

                }

                Card card = new Card(suit, i, isWildcard);
                cards.Add(card);
            }

            deck = new SuitDeck(cards, suit);

        }
    }
}
