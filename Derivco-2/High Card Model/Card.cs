﻿
namespace Question2
{
    public class Card
    {
        private CardSuit _suit;
        private bool _isWildCard;
        private int _cardValue;

        public Card(CardSuit suit, int cardValue, bool isWildcard)
        {
            _cardValue = cardValue;
            _isWildCard = isWildcard;
            _suit = suit;
        }

        public CardSuit Suit => _suit;
        public bool IsWildcard => _isWildCard;
        public int CardValue => _cardValue;
    }
}
