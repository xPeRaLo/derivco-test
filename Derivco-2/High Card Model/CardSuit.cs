﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
	public enum CardSuit
	{
		CLUBS = 0, 
		DIAMONDS = 1, 
		HEARTS = 2,
		SPADES = 3
	}
}
