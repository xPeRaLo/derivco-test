﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
    public class SuitDeck
    {
        CardSuit _suit;
        List<Card> _cards;

        public SuitDeck(List<Card> cards, CardSuit suit)
        {
            _suit = suit;
            _cards = cards;
        }

        public CardSuit Suit => _suit;
        public List<Card> Cards => _cards;

    }
}
