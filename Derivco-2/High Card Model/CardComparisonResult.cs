﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
	public enum CardComparisonResult
	{
		LOWER,
		HIGHER,
		LOWERBYWILDCARD,
		LOWERBYSUIT,
		HIGHERBYWILDCARD,
		HIGHERBYSUIT,
		DRAW,
	}
}
