﻿namespace Question2
{
    public static class HighCardExtensions
    {
        public static CardComparisonResult Compare(this Card self, Card compareTo)
        {
            CardComparisonResult result = CardComparisonResult.DRAW;

            // Wildcard Comparison

            if (self.IsWildcard || compareTo.IsWildcard)
            {
                if (self.IsWildcard && !compareTo.IsWildcard)
                {
                    return CardComparisonResult.HIGHERBYWILDCARD;
                }
                else if (!self.IsWildcard && compareTo.IsWildcard)
                {
                    return CardComparisonResult.LOWERBYWILDCARD;
                }
            }

            // Value comparison

            if (self.CardValue > compareTo.CardValue)
            {
                result = CardComparisonResult.HIGHER;
            }
            else if (self.CardValue < compareTo.CardValue)
            {
                result = CardComparisonResult.LOWER;
            }

            // Suit Comparison

            if(self.CardValue == compareTo.CardValue)
			{
                bool? isSuiteHigher = self.IsSuitHigher(compareTo);

                if (isSuiteHigher.HasValue)
                {
                    if (isSuiteHigher.Value == true) { result = CardComparisonResult.HIGHERBYSUIT; }
                    else { result = CardComparisonResult.LOWERBYSUIT; }
                }
            }

            return result;
        }

        public static bool TryGetAvailableCardDeck(this HighCard self, out CardDeck deck)
        {
            deck = null;

            if(self.Decks[0].GetTotalCards() < 2)
			{
                self.Decks.RemoveAt(0);
               
			}

            if (self.Decks != null && self.Decks.Count > 0)
            {
                deck = self.Decks[0];
                return true;
            }

            return false;
        }

        public static int GetTotalCards(this CardDeck self)
		{
            return (self.ClubDeck.Cards.Count + 
                    self.DiamondDeck.Cards.Count + 
                    self.HeartDeck.Cards.Count + 
                    self.SpadeDeck.Cards.Count);
		}

        public static bool IsEmpty(this SuitDeck self)
		{
            if (self.Cards.Count > 0) return false;

            return true;
		}

        public static bool? IsSuitHigher(this Card self, Card compareTo)
		{
            int selfSuitValue = (int) self.Suit;
            int compareToSuitValue = (int) compareTo.Suit;

            if(selfSuitValue > compareToSuitValue) { return true; }
            if(selfSuitValue < compareToSuitValue) { return false; }

            return null;

		}
    }
}
