﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Question2
{
    public class HighCard
    {
        private List<CardDeck> _decks = new List<CardDeck>();

        private Random _random;
        private const int RANDOM_SEED = 14515419;
        private CardDeck _currentDeck;

        public HighCard(int numberOfDecks)
        {
            _random = new Random(RANDOM_SEED);

            for (int i = 0; i < numberOfDecks; i++)
            {
                _decks.Add(new CardDeck());
            }
        }

        public List<CardDeck> Decks => _decks;

        public void Play(HighCard highCard)
        {
            if (!highCard.TryGetAvailableCardDeck(out _currentDeck))
			{
                Console.WriteLine("Game finished beacause all decks were used.");
                return;
            }

            Card[] cards = new Card[2];
            
            for(int i = 0; i < cards.Length; i++)
			{
                if(!TryGetCard(_currentDeck, out cards[i]))
				{
                    while(cards[i] == null)
					{
                        TryGetCard(_currentDeck, out cards[i]);
                    }
                    
				}
			}

            if(cards[0] != null && cards[1] != null)
			{
                CardComparisonResult result = cards[0].Compare(cards[1]);
                DetermineWinner(cards, result);
            }
        }

        private bool TryGetCard(CardDeck deck, out Card card)
        {
            int c;
            int s = _random.Next(4);

            card = null;

            switch (s)
            {
                case 0:
                    if (!deck.ClubDeck.IsEmpty())
                    {
                        c = _random.Next(deck.ClubDeck.Cards.Count);
                        card = deck.ClubDeck.Cards[c];
                        deck.ClubDeck.Cards.RemoveAt(c);
                    }

                    break;
                case 1:
                    if (!deck.DiamondDeck.IsEmpty())
                    {
                        c = _random.Next(deck.DiamondDeck.Cards.Count);
                        card = deck.DiamondDeck.Cards[c];
                        deck.DiamondDeck.Cards.RemoveAt(c);
                    }
                    break;
                case 2:
                    if (!deck.HeartDeck.IsEmpty())
                    {
                        c = _random.Next(deck.HeartDeck.Cards.Count);
                        card = deck.HeartDeck.Cards[c];
                        deck.HeartDeck.Cards.RemoveAt(c);
                    }
                    break;
                case 3:
                    if (!deck.SpadeDeck.IsEmpty())
                    {
                        c = _random.Next(deck.SpadeDeck.Cards.Count);
                        card = deck.SpadeDeck.Cards[c];
                        deck.SpadeDeck.Cards.RemoveAt(c);
                    }
                    break;
                default:
                    break;
            }

            if (card != null)
            {
                return true;
            }

            return false;
        }

        private void DetermineWinner(Card[] cards, CardComparisonResult result)
		{
            Thread.Sleep(250);
            string text = "";

            Card card1 = cards[0];
            Card card2 = cards[1];


            switch (result)
			{
                case CardComparisonResult.DRAW:
                    text = "Result is a draw.";
                    break;
                case CardComparisonResult.HIGHER:
                    text = $"{card1.CardValue} of {card1.Suit} beats {card2.CardValue} of {card2.Suit}.";
                    break;
                case CardComparisonResult.HIGHERBYWILDCARD:
                    text = $"{card1.CardValue} of {card1.Suit} beats {card2.CardValue} of {card2.Suit} because it's a wildcard.";
                    break;
                case CardComparisonResult.HIGHERBYSUIT:
                    text = $"{card1.CardValue} of {card1.Suit} beats {card2.CardValue} of {card2.Suit} because the suit is valued more.";
                    break;
                case CardComparisonResult.LOWER:
                    text = $"{card1.CardValue} of {card1.Suit} loses to {card2.CardValue} of {card2.Suit}.";
                    break;
                case CardComparisonResult.LOWERBYWILDCARD:
                    text = $"{card1.CardValue} of {card1.Suit} loses to {card2.CardValue} of {card2.Suit} because it's a wildcard.";
                    break;
                case CardComparisonResult.LOWERBYSUIT:
                    text = $"{card1.CardValue} of {card1.Suit} loses to {card2.CardValue} of {card2.Suit} because the suit is valued less.";
                    break;
            }

            Console.WriteLine(text+"\n");
            Play(this);
			 
		}
    }
}
